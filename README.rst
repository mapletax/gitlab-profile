#############
MapleTax 🍁💸
#############


:[Matrix]: `#mapletax:zougloub.eu <https://matrix.to/#/#mapletax:zougloub.eu>`_

:Activity: https://gitlab.com/groups/mapletax/-/activity

:Main Repo: https://gitlab.com/mapletax/mapletax/

:Wiki: https://gitlab.com/mapletax/mapletax/-/wikis/

:Sponsors: `ExMakhina <https://gitlab.com/exmakhina>`_


MapleTax is a piece of Free Software aiming to provide the means
to do Canadian tax declarations, that is, without doing it by hand,
using proprietary software, or having to rely on an accountant for simple
situations.


Status
######

As of now, the solution is not yet usable, but it's getting there,
you can follow up on the progress by checking out the activity feed,
or hanging out on the [Matrix] discussion room.

**We're looking for contributors!**
